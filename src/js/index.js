import Glider from 'glider-js'

const menu = document.querySelector('.menu')
const nav = document.querySelector('nav')
const form = document.querySelector('form')
const cardContainer = document.querySelector('.cards')

menu.addEventListener('click', function () {
    nav.classList.toggle('active')
    menu.classList.toggle('straight')
    menu.classList.toggle('cross')
})

form.addEventListener('submit', function (e) {
    e.preventDefault()
    const submittedEmail = form.email
    submittedEmail.classList.remove('invalid')
    console.log(submittedEmail)

    if (!ValidateEmail(submittedEmail.value) || submissionEmail.value == '') submittedEmail.classList.add('invalid')
})

window.addEventListener('DOMContentLoaded', () => {
    const glider = new Glider(cardContainer, {
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: true,
        dots: '.dots',
        responsive: [
            {
                // screens greater than >= 775px
                breakpoint: 775,
                settings: {
                    // Set to `auto` and provide item width to adjust to viewport
                    slidesToShow: 'auto',
                    slidesToScroll: 'auto',
                    itemWidth: 330,
                    // duration: 0.25,
                },
            },
            {
                // screens greater than >= 1024px
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2.75,
                    slidesToScroll: 1,
                    itemWidth: 540,
                    // duration: 0.25,
                },
            },
        ],
    })
})

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) return true

    return false
}
